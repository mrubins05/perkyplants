# Perky Plants

By: Maya Rubins

---slide---

## Project Overview

---slide---

#### The Product

<p> Perky Plants is a website designed to sell plants online.</p>

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/pphomepage.png" width="35%">

---slide---

#### Project Duration

<p>June 2022 - September 2022</p>

---slide---


#### The Problem

<p> Plants are a hard thing to purchase online because they differ alot from plant to plant.</p> 

---slide---

#### The Goal

<p> I wanted to make plants more accessible for people online so they can purchase one while making sure that they know how to care for it.</p>

---slide---

#### My Role

<p> I was the lead UX designer and UX researcher for this project.</p>

---slide---

#### My Responsibilities
* Conducted user interviews
* Created paper and digital wireframes
* Developed low and high-fidelity prototypes
* Conducted usability studies

---slide---

## Understanding the User
***
* User Research
* Personas
* Problem Statements
* User Journey Maps

---slide---

## User Research

---slide---

#### Summary

<p>I conducted user interviews and created empathy maps to better understand the users I was designing for and their needs.</p>

---slide---

#### Primary User Group

<p>Individuals who wanted to buy a plant online last minute as a gift.</p>

---slide---

#### Results

<p>The user research revealed that deciding on a plant, choosing the right plant for their living environment, and knowledge on how to care for the plant were all limiting factors for customers.</p>

---slide---

#### User Pain Points

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/pppainpoints.png" width="100%">

---slide---

## Personas

---slide---

#### Meet Allison

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/allison.png" width="100%">

---slide---

## Problem Statement

---slide---

#### Allison's Problem

<p> Allison is a mom of two new born children, and needs to qickly find a plant for her moms birthday tomorrow.</p>

---slide---

## User Journey Map

---slide---

#### Allison's Journey Map

<p> Mapping Allison's journey revealed how helpful it would be for users to save time finding their perfect plant.</p>

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/allisonchart.png" width="100%">

---slide---

## Starting the Design
***
* Sitemap
* Paper Wireframes
* Digital Wireframes
* Low-fidelity Prototypes
* Usability Studies

---slide---

## Sitemap

---slide---

#### Site Overview
* Search for a plant
* Learn more on an about page
* Shop for a plant
* Access the cart for the purchase of a plant

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/sitemap.png" width="45%">

---slide---

## Paper Wireframes

---slide---

#### Wireframing Process

<p> Drafting iterations of the app on paper ensured that the elements that made it to digital wireframes would be able to address the users pain points.</p>

---slide---

#### Initial Wireframes

<p>Stars were used to mark the elements of each sketch that would be used in the digital wireframes.</p>

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe1.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe2.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe3.png" width="30%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe4.png" width="30%">

---slide---

#### Finalized Wireframes

<p> For the final draft of the home screen, I prioritized a <strong>new arrivals and search function</strong> to help users save time.</p>

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe5.png" width="30%">

---slide---

#### Screen Size Variation

<p>When designing wireframes for a bigger screen size necessary adjustments were made, but the overall concept remains the same.</p>

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppwireframe6.png" width="30%">

---slide---

## Digital Wireframes

---slide---

#### Inital Digital Wireframes

<p> As the initial design phase continued, I made sure to create the screen designs based on feedback and findings from the user research.</p>

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/digitalppwire.png" width="75%">

---slide---

#### Screen Size Variation

<p>For the large screen digital wireframe, I removed the navigation page altogether and added tabs for navigation in the upper right-hand corner of every screen.</p>

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/digitalppwire1.png" width="80%">

---slide---

## Low-Fidelity Prototype

---slide---

#### Summary

<p>Using the completed set of digital wireframes, I created a low-fidelity prototype. The primary user flow was encouraging users to select and purchase a product.</p>

---slide---

<video width="80%" data-autoplay src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/pplowfi.mov"></video>

---slide---

## Usability Study

---slide---

#### Parameters

---slide---

#### Study Type

<p>Unmoderated usability study</p>

---slide---

#### Location

<p>United States, remote</p>

---slide---

#### Participants

<p>5 participants</p>

---slide---

#### Length

<p>20-30 minutes</p>

---slide---

#### Summary

<p> The usability study showed me that there were still some areas of the website that needed refining, so the customer could increase the speed at which they were able to shop and care for the right plant.</p>

---slide---

#### Findings
* Users want to be able to save their information for a faster checkout.
* Users want tips on how to care for thier plant.
* Users want help sorting through plants to find the right one for their home or environment.

---slide---

## Refining the Design
***
* Mockups
* High-fidelity Prototypes
* Accessibility

---slide---

## Mockups

---slide---

#### Summary

<p>The users during the usability study were disappointed that the navigation page wasn't allowing them to see important pages such as the <strong>about us, my orders, or my returns</strong> pages.</p>

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/beforepp.png" width="35%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/afterpp.png" width="35%">

---slide---

#### Summary Sreen Size Variation

<p>During the usability study users pointed out how it wouldn't make sense to have the<strong> accounts, favorites, search, and cart</strong> icons on the login page if the user wasn't even logged into the website.<p>

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/beforepp1.png" width="50%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/afterpp1.png" width="50%">

---slide---

#### Finalized Mockups: Original Screen Size

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/bigppaccount.png" width="60%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/bigpplogin.png" width="60%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/bigppfavorites.png" width="50%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/bigpphomepage.png" width="45%">

---slide---

#### Finalized Mockups: Screen Size Variation

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/smallppaccount.png" width="20%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/smallpplogin.png" width="20%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/smallppfavorites.png" width="20%">

---slide---

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/smallpphomepage.png" width="20%">

---slide---

## High-fidelity Prototype

---slide---

#### Summary

<p> The final high-fidelity prototype met user needs with an added navigation page for smaller screens. As well as added color and texture to meet accessibility standards.</p>

---slide---

<video width="80%" data-autoplay src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/pphighfi.mov"></video>


---slide---

## Accessibility Considerations

---slide---

#### Conserns to Consider

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppaccessibility.png" width="100%">


---slide---

## Going Forward
***
* Take Aways
* Next Steps

---slide---

## Take Aways

---slide---

#### Impact

<p> My designs not only impacted how people can shop and purchase plants, but it also can impact how people care for them.</p>

---slide---

#### Quote from User Feedback

_"Perky Plants makes shopping for a plant easy and simple."_

---slide---

#### What I Learnt

<p>Throughout this project I learnt how to work with Adobe XD to create digital wireframes and to create both low and high-fidelity prototypes based on user research and feedback.</p>

---slide---

## Next Steps

---slide---

#### The Future of Perky Plants

<img src="https://gitlab.com/mrubins05/perkyplants/-/raw/main/images/ppnextsteps.png" width="100%">

---slide---

## Let's Connect!

---slide---

#### Stay in Touch

<p> Thank you for reviewing my work on the Perky Plants website! If you'd like to see more or get in touch, my contact information is provided on the next page.</p>

---slide---

<section data-auto-animate>

#### Reach Out

<div data-id="maya">

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:50%;">
</div>
</section>

---slide---

<section data-auto-animate>

#### Reach Out

<div data-id="maya">

<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:40%;">
</div>

<div data-id="contact">

<a href="mailto: maya.rubins@gmail.com">Send Email</a>
</div>
</section>

---slide---

##### Reach Out

<section data-auto-animate>

<div data-id="maya">
<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:30%;">
</div>
<div data-id="contact">

<a href="mailto: maya.rubins@gmail.com">Send Email</a>

<a href="https://mrubins05.gitlab.io/portfolio/index.html">My Website</a>
</div>
</section>

---slide---

#### Reach Out

<section data-auto-animate>

<div data-id="maya">
<img src="https://gitlab.com/mrubins05/foodventory/-/raw/main/images/maya1.jpg" style="width:20%;">
</div>

<div data-id="contact">

<a href="mailto: maya.rubins@gmail.com">Send Email</a>

<a href="https://mrubins05.gitlab.io/portfolio/index.html">My Website</a>

<a href="https://xd.adobe.com/view/c953b83c-3382-46cb-a8f8-9a763fb3be10-9c00/">Perky Plants Website</a>
</div>
</section>

---slide---

# Thank You!
***

 <a href="https://mrubins05.gitlab.io/portfolio/index.html"><p>Back to my Portfolio :)</p>
 </a>
